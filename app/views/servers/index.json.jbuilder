json.array!(@servers) do |server|
  json.extract! server, :id, :name, :instance_id, :ip_address, :cpu_usage, :disk_usage
  json.url server_url(server, format: :json)
end
