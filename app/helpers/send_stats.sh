#!/usr/bin/env bash
#sh send_stats.sh 54.191.136.222 574b1a1c8690504ee6000000
for i in {1..11}
do
    disk_usage=$(df -h / |grep -v Use% | awk '{print $5}' | cut -c 1-2)
    cpu_usage=$(grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage}')
    curl -H "Content-Type: application/json" -X PUT -d '{"cpu_usage" : '"$cpu_usage"', "disk_usage" : '"$disk_usage"'}' http://$1/servers/$2.json
    sleep 5
done
