class ServersController < ApplicationController
  before_action :set_server, only: [:show, :show_processes, :edit, :update, :destroy]

  # GET /servers
  # GET /servers.json
  api :GET, '/servers.json'
  description 'Get all servers
  [
      {
          "id": {
              "$oid": "5749310343c6572eba000001"
          },
          "name": "Second Server",
          "instance_id": "id2",
          "ip_address": "hytjuyk",
          "cpu_usage": null,
          "disk_usage": null,
          "url": "http://localhost:3000/servers/5749310343c6572eba000001.json"
      },
      {
          "id": {
              "$oid": "5749317943c6572eba000002"
          },
          "name": "3rd_server",
          "instance_id": "hvgwjkkdc",
          "ip_address": "62.2.2n2.",
          "cpu_usage": 1.2,
          "disk_usage": 86,
          "url": "http://localhost:3000/servers/5749317943c6572eba000002.json"
      },
      {
          "id": {
              "$oid": "5749ad2143c6574aba000002"
          },
          "name": "FOurth",
          "instance_id": "ugghjknml",
          "ip_address": "hbnm",
          "cpu_usage": null,
          "disk_usage": null,
          "url": "http://localhost:3000/servers/5749ad2143c6574aba000002.json"
      },
      {
          "id": {
              "$oid": "5749b13443c6574aba000003"
          },
          "name": "fift_server",
          "instance_id": "hvgwdc",
          "ip_address": "52.33.139.194",
          "cpu_usage": null,
          "disk_usage": null,
          "url": "http://localhost:3000/servers/5749b13443c6574aba000003.json"
      },
      {
          "id": {
              "$oid": "574af46143c657dfa4000000"
          },
          "name": "ytghj",
          "instance_id": "gvghbj",
          "ip_address": "hgvhjbn1a",
          "cpu_usage": null,
          "disk_usage": null,
          "url": "http://localhost:3000/servers/574af46143c657dfa4000000.json"
      }
  ]'
  def index
    @servers = Server.all
  end

  # GET /servers/1
  # GET /servers/1.json
  api :GET, '/servers/{:id}.json'
  description 'Get server with id = params[:id]'
  def show
  end

  def show_processes
    processes = `ssh #{@server.ssh_user}@#{@server.ip_address} ps aux`
    @processes = processes.split(" ")
  end

  # GET /servers/new
  def new
    @server = Server.new
  end

  # GET /servers/1/edit
  def edit
  end

  # POST /servers
  # POST /servers.json
  api :POST, '/servers.json'
  description 'Create new server'
  def create
    @server = Server.new(server_params)

    respond_to do |format|
      if @server.save
        format.html { redirect_to @server, notice: 'Server was successfully created.' }
        format.json { render :show, status: :created, location: @server }
      else
        format.html { render :new }
        format.json { render json: @server.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /servers/1
  # PATCH/PUT /servers/1.json
  api :PUT, '/servers/{:id}.json'
  description 'Update server with id = param[:id]'
  def update
    respond_to do |format|
      @server.ip_address
      if @server.update(server_params)
        format.html { redirect_to @server, notice: 'Server was successfully updated.' }
        format.json { render :show, status: :ok, location: @server }
      else
        format.html { render :edit }
        format.json { render json: @server.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /servers/1
  # DELETE /servers/1.json
  def destroy
    @server.destroy
    respond_to do |format|
      format.html { redirect_to servers_url, notice: 'Server was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_server
      @server = Server.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def server_params
      params.require(:server).permit(:name, :instance_id, :ip_address, :ssh_user, :cpu_usage, :disk_usage)
    end
end
