class Server
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name
  field :instance_id
  field :ip_address
  field :ssh_user
  field :cpu_usage, type: Float
  field :disk_usage, type: Float

  embeds_many :attribute_histories
  validates_uniqueness_of :name, :instance_id, :ip_address

  before_update :save_attribute_history

  def save_attribute_history
    self.attribute_histories.create!({:attribute_name => 'cpu_usage',
                                                        :attribute_value => self.cpu_usage,
                                                        :created_at => Time.now})
    self.attribute_histories.create!({:attribute_name => 'disk_usage',
                                                        :attribute_value => self.disk_usage,
                                                        :created_at => Time.now})
  end

  def cpu_usage_history
    self.attribute_histories.select{|h| h['attribute_name']=='cpu_usage'}.last(10)
  end

  def disk_usage_history
    self.attribute_histories.select{|h| h['attribute_name']=='disk_usage'}.last(10)
  end

end
