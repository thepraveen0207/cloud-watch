class AttributeHistory
  include Mongoid::Document

  include Mongoid::Timestamps
  field :attribute_name
  field :attribute_value
  # field :created_at, :type => DateTime

  embedded_in :server, :inverse_of => :attribute_histories
end